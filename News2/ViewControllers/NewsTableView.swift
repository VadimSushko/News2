//
//  ViewController.swift
//  News2
//
//  Created by Vadym Sushko on 07.09.2021.
//
import Combine
import UIKit

class NewsTableView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var posts = [Post]()
    var netManager = NetworkManager()
    var observer: AnyCancellable?
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
        observer = netManager.fetchData()
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] result in
                self?.posts = result.hits
                self?.myTableView.reloadData()
            })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let post = posts[indexPath.row]
        cell.textLabel?.text = String(post.points) + "  " + post.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showArticle" {
            let vc = segue.destination as! DetailsViewController
            if let selectedIndex = myTableView.indexPathForSelectedRow {
                vc.urlString = posts[selectedIndex.row].url
            }
        }
    }
    
}

