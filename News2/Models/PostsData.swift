//
//  PostsData.swift
//  News
//
//  Created by Vadym Sushko on 02.09.2021.
//

import Foundation

struct Results: Decodable {
    let hits: [Post]
}

struct Post: Decodable {
    let objectID: String
    let points: Int
    let title: String
    let url: String?
}
