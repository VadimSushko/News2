//
//  NetworkManager.swift
//  News
//
//  Created by Vadym Sushko on 02.09.2021.
//

import Combine
import Foundation

class NetworkManager {
        
    func fetchData() -> AnyPublisher<Results, Never> {
        
        let url = URL(string: "https://hn.algolia.com/api/v1/search?tags=front_page")
            
        let publisher = URLSession.shared.dataTaskPublisher(for: url!)
                .map({$0.data})
                .decode(type: Results.self, decoder: JSONDecoder())
                .catch( { _ in
                    Just(Results(hits: []))
                })
                .eraseToAnyPublisher()
        
            return publisher
    }
}

